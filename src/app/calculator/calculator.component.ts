import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import { ICalculatorButton } from './core/calculator-button.interface';
import { CalculatorButtonType } from './core/calculator-button-type.enum';
import { CalculatorOperationType } from './core/calculator-operation-type.enum';
import { CalculatorResetType } from './core/calculator-reset-type.enum';
import { CalculatorService } from './calculator.service';
import { createNumberMask } from './core/text-mask/index';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit, AfterViewInit {

  // Calculator input element reference.
  @ViewChild('calculatorInput')
  public calculatorInput: ElementRef;

  // Current value in the input
  public currentValue: string;

  // Indicates if a digit button has been clicked
  public currentValueChanged: boolean;

  // Previous value from the input
  public previousValue: string;

  // Indicates the last operation button clicked
  public operationValue: null | number | '.' | CalculatorResetType | CalculatorOperationType;

  // Indicates if the last clicked button was operation type
  public operationActive: boolean;

  // Array with calculation results
  public resultsArray: string[];

  // Array with calculation results
  public resultsArrayDropped: string[];

  // Button list
  public buttonList: ICalculatorButton[][];

  // Button type enum instance
  public calculatorButtonType = CalculatorButtonType;

  // Button operation type enum instance
  public calculatorOperationType = CalculatorOperationType;

  // Button reset type instance
  public calculatorResetType = CalculatorResetType;

  // Mask for the input
  // Not used because it doesn't allow setting 0. in the input, so the input is disabled
  public mask: any;

  /**
   * Creates a new instance of CalculatorComponent
   *
   * @param calculatorService service injection
   */
  public constructor(private calculatorService: CalculatorService) {

  }

   /**
	 * Lifecycle hook that is called when a component is initialized.
	 */
  public ngOnInit(): void {
    this.currentValue = '0';
    this.currentValueChanged = false;
    this.previousValue = '0';

    this.operationValue = null;
    this.operationActive = false;
    this.resultsArray = [];
    this.resultsArrayDropped = [];

    this.buttonList = this.getConfig();

    this.mask = createNumberMask({
      prefix: '',
      suffix: '',
      allowDecimal: true,
      decimalLimit: 10,
      decimalSymbol: '.',
      allowNegative: true,
      integerLimit: 10
    });
  }

  /**
	 * Lifecycle hook that is called after Angular has fully initialized a component's view.
	 */
  public ngAfterViewInit(): void {
    this.calculatorInput.nativeElement.focus();
  }

  /**
   * Get the button list from the service
   */
  public getConfig() {
    return this.calculatorService.getConfig();
  }

  /**
   * Update input / Revert input / Calculate
   *
   * @param button object
   */
  public onClick(button: ICalculatorButton): void {


    switch (button.type) {

      // Update the current value
      case this.calculatorButtonType.Digit:
        const prevValue = this.operationActive ? '0' : this.currentValue;
        this.currentValue = button.value === '.' ? prevValue + '.' : parseFloat(prevValue + '' + button.value) + '';
        this.operationActive = false;
        this.currentValueChanged = true;
        break;

      // Revert the current and the previous values
      case this.calculatorButtonType.Revert:
        switch (button.value) {
          case this.calculatorResetType.Back:
            if (this.resultsArray.length) {
              this.resultsArray.splice(-1, 1);
            }
            if (this.resultsArray.length) {
              this.currentValue = this.resultsArray[this.resultsArray.length - 1];
            } else {
              this.currentValue = '0';
            }
            this.previousValue = this.currentValue;
            break;
          case this.calculatorResetType.ClearAll:
            this.currentValue = '0';
            this.resultsArray = [];
            this.previousValue = this.currentValue;
            break;
          case this.calculatorResetType.Clear:
            this.currentValue = '0';
            break;
        }
        break;

      // Calculate
      case this.calculatorButtonType.Operation:
        if (this.currentValueChanged || this.operationValue === this.calculatorOperationType.Evaluate) {
          if (this.operationValue !== null) {
            const previousValueFloat = parseFloat(this.previousValue),
              currrentValueFloat = parseFloat(this.currentValue);
            switch (this.operationValue) {
              case this.calculatorOperationType.Divide:
                this.currentValue = this.calculatorService.divide(previousValueFloat, currrentValueFloat) + '';
                break;
              case this.calculatorOperationType.Multiply:
                this.currentValue = this.calculatorService.multiply(previousValueFloat, currrentValueFloat) + '';
                break;
              case this.calculatorOperationType.Plus:
                this.currentValue = this.calculatorService.plus(previousValueFloat, currrentValueFloat) + '';
                break;
              case this.calculatorOperationType.Minus:
                this.currentValue = this.calculatorService.minus(previousValueFloat, currrentValueFloat) + '';
                break;
            }
          }
          this.operationValue = button.value;
          this.operationActive = true;

          // In case of evaluate, add the result to the array
          if (button.value === this.calculatorOperationType.Evaluate) {
            this.resultsArray.push(this.currentValue);
          }
          this.previousValue = this.currentValue;
          this.currentValueChanged = false;
        }
        break;
    }
  }

  /**
   * Drag an item from a list to other
   *
   * @param event CdkDragDrop<string[]>
   */
  public drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }
}



