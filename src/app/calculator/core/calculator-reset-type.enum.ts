/**
 * Calculator's reset type
 */
export enum CalculatorResetType {

    // Goes back to the previous result
    Back = 'back',

    // Clears the current input
    Clear = 'clear',

    // Clears all the results
    ClearAll = 'clearall'
}
