/**
 * Calculator's operation type
 */
export enum CalculatorOperationType {

    // +
    Plus = 'plus',

    // -
    Minus = 'minus',

    // X
    Multiply = 'multiply',

    // /
    Divide = 'divide',

    // =
    Evaluate = 'evaluate'
}
