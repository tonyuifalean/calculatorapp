import { ICalculatorButton } from './calculator-button.interface';
import { CalculatorButtonType } from './calculator-button-type.enum';
import { CalculatorResetType } from './calculator-reset-type.enum';
import { CalculatorOperationType } from './calculator-operation-type.enum';

/**
 * Caculator's button config
 * Each array represents a line with buttons
 */
export const CalculatorConfig: ICalculatorButton[][] = [
  [
    {
      text: 'Back',
      type: CalculatorButtonType.Revert,
      value: CalculatorResetType.Back
    },
    {
      text: 'AC',
      type: CalculatorButtonType.Revert,
      value: CalculatorResetType.ClearAll
    },
    {
      text: 'C',
      type: CalculatorButtonType.Revert,
      value: CalculatorResetType.Clear
    },
    {
      text: '/',
      type: CalculatorButtonType.Operation,
      value: CalculatorOperationType.Divide
    }
  ],
  [
    {
      text: '7',
      type: CalculatorButtonType.Digit,
      value: 7
    },
    {
      text: '8',
      type: CalculatorButtonType.Digit,
      value: 8
    },
    {
      text: '9',
      type: CalculatorButtonType.Digit,
      value: 9
    },
    {
      text: 'X',
      type: CalculatorButtonType.Operation,
      value: CalculatorOperationType.Multiply
    }
  ],
  [
    {
      text: '4',
      type: CalculatorButtonType.Digit,
      value: 4
    },
    {
      text: '5',
      type: CalculatorButtonType.Digit,
      value: 5
    },
    {
      text: '6',
      type: CalculatorButtonType.Digit,
      value: 6
    },
    {
      text: '-',
      type: CalculatorButtonType.Operation,
      value: CalculatorOperationType.Minus
    }
  ],
  [
    {
      text: '1',
      type: CalculatorButtonType.Digit,
      value: 1
    },
    {
      text: '2',
      type: CalculatorButtonType.Digit,
      value: 2
    },
    {
      text: '3',
      type: CalculatorButtonType.Digit,
      value: 3
    },
    {
      text: '+',
      type: CalculatorButtonType.Operation,
      value: CalculatorOperationType.Plus
    }
  ],
  [
    {
      text: '0',
      type: CalculatorButtonType.Digit,
      value: 0
    },
    {
      text: '.',
      type: CalculatorButtonType.Digit,
      value: '.'
    },
    {
      text: '=',
      type: CalculatorButtonType.Operation,
      value: CalculatorOperationType.Evaluate
    }
  ]
];
