import { CalculatorButtonType } from './calculator-button-type.enum';
import { CalculatorResetType } from './calculator-reset-type.enum';
import { CalculatorOperationType } from './calculator-operation-type.enum';

/**
 * Interface for button
 */
export interface ICalculatorButton {
    // Text displayed on the button
    text: string;

    // Button's type
    type: CalculatorButtonType;

    // Button's value
    value: number | '.' | CalculatorResetType | CalculatorOperationType;
}
