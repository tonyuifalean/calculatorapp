/**
 * Calculator's button type
 */
export enum CalculatorButtonType {

    // 0-9, -
    Digit = 'digit',

    // /, X, -, +
    Operation = 'operation',

    // Back, AC, C
    Revert = 'revert'
}
