import { Injectable } from '@angular/core';
import { CalculatorConfig } from './core/calculator-config';
import { ICalculatorButton } from './core/calculator-button.interface';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  public constructor() { }

  public getConfig(): ICalculatorButton[][] {
    return CalculatorConfig;
  }

    /**
   * Add operation
   *
   * @param val1 first value
   * @param val2 second value
   */
  public plus(val1: number, val2: number): number {
    return val1 + val2;
  }

  /**
   * Deduct operation
   *
   * @param val1 first value
   * @param val2 second value
   */
  public minus(val1: number, val2: number): number {
    return val1 - val2;
  }

  /**
   * Multiply operation
   *
   * @param val1 first value
   * @param val2 second value
   */
  public multiply(val1: number, val2: number): number {
    return val1 * val2;
  }

  /**
   * Divide operation
   *
   * @param val1 first value
   * @param val2 second value
   */
  public divide(val1: number, val2: number): number {
    return val1 / val2;
  }
}
