import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CalculatorComponent } from './calculator.component';
import { TextMaskModule } from 'angular2-text-mask';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    CalculatorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TextMaskModule,
    DragDropModule,
    RouterModule.forChild([
      {
        path: '',
        component: CalculatorComponent
      },
    ])
  ]
})
export class CalculatorModule { }
