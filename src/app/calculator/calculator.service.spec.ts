import { TestBed } from '@angular/core/testing';

import { CalculatorService } from './calculator.service';

describe('CalculatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalculatorService = TestBed.get(CalculatorService);
    expect(service).toBeTruthy();
  });

  it('should make an accurate calculation', () => {
    const service: CalculatorService = TestBed.get(CalculatorService),
      result = service.divide(service.minus(service.multiply(service.plus(1, 2), 3), 0.2), 2);
    expect(result).toBe(4.4);
  });
});
