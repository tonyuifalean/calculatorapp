import { CalculatorComponent } from './calculator.component';
import { CalculatorService } from './calculator.service';
import { CalculatorButtonType } from './core/calculator-button-type.enum';
import { CalculatorResetType } from './core/calculator-reset-type.enum';

describe('CalculatorComponent', () => {
  let component: CalculatorComponent,
    service: CalculatorService;

    beforeEach(() => {
      service = new CalculatorService();
      component = new CalculatorComponent(service);
    });

    it('should return 3 lines of keys', () => {
      const buttonList = [[
        {
          text: 'Button 1',
          type: CalculatorButtonType.Revert,
          value: CalculatorResetType.Back
        },
        {
          text: 'Button 2',
          type: CalculatorButtonType.Revert,
          value: CalculatorResetType.ClearAll
        },
        {
          text: 'Button ',
          type: CalculatorButtonType.Revert,
          value: CalculatorResetType.Clear
        }]];

      spyOn(service, 'getConfig').and.callFake(() => {
        return buttonList;
      });

      component.ngOnInit();

      expect(component.buttonList).toBe(buttonList);
    });
});
